package cz.forum.services;

import cz.forum.dao.ArticleDao;
import cz.forum.dao.CommentDao;
import cz.forum.dao.UserDao;
import cz.forum.model.Role;
import cz.forum.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private ArticleDao articleDao;

    @Autowired
    private CommentDao commentDao;

    @Override
    public User getAuthUser() {
        if (isUserAuthenticated()) {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            return userDao.findByEmail(auth.getName());
        }
        return null;
    }

    @Override
    public boolean isUserAuthenticated() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return userDao.findByEmail(auth.getName()) == null ? false : true;
    }

    @Override
    public boolean userIsAdmin() {
        if (isUserAuthenticated()) {


            Set<Role> role = getAuthUser().getRoles();
            for (Role one : role) {
                if (one.getId() == 1) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean userHasPersmissionsOnArticle(long id) {
        return articleDao.findById(id).getUser().getId() == getAuthUser().getId();
    }

    @Override
    public boolean userHasPersmissionsOnComment(long id) {
        return commentDao.findById(id).getUser().getId() == getAuthUser().getId();
    }

}
