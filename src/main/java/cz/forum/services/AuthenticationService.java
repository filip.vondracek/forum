package cz.forum.services;

import cz.forum.model.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

public interface AuthenticationService {

    User getAuthUser();

    boolean isUserAuthenticated();

    boolean userIsAdmin();

    boolean userHasPersmissionsOnArticle(long id);

    boolean userHasPersmissionsOnComment(long id);

}
