package cz.forum.controllers;

import cz.forum.dao.UserDao;
import cz.forum.model.Article;
import cz.forum.model.User;
import cz.forum.services.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import javax.validation.Valid;

@Controller
public class UserController {

    @Autowired
    private UserDao userDao;

    @Autowired
    private AuthenticationService authenticationService;

    @RequestMapping(value="/admin/users", method = RequestMethod.GET)
    public ModelAndView users(@RequestParam(value="user_id",required=false) Integer id) {
        ModelAndView modelAndView = new ModelAndView();

        if (id != null) {
            userDao.remove(id);
            modelAndView.addObject("successMessage", "User has been deleted successfully");
        }

        modelAndView.addObject("users", userDao.findAll());
        modelAndView.setViewName("admin/users");
        modelAndView.addObject("admin", authenticationService.userIsAdmin());
        return modelAndView;
    }

    @RequestMapping(value="/admin/user", method = RequestMethod.GET)
    public ModelAndView user(@RequestParam(value="user_id",required=false) Integer id){
        ModelAndView modelAndView = new ModelAndView();
        if (id == null) {
            modelAndView.addObject("user", new User());
            modelAndView.addObject("buttonType", "Add");
        } else {
            modelAndView.addObject("user", userDao.findById(id));
            modelAndView.addObject("buttonType", "Edit");
        }
        modelAndView.setViewName("admin/user");
        modelAndView.addObject("admin", authenticationService.userIsAdmin());
        return modelAndView;
    }

    @RequestMapping(value = "/admin/user", method = RequestMethod.POST)
    public ModelAndView createNewUser(@Valid User user, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();

        if (bindingResult.hasErrors()) {

            modelAndView.addObject("buttonType", "Add");
            modelAndView.setViewName("admin/user");
        } else {
            if (user.getId() != 0) {
                userDao.update(user);
                modelAndView.addObject("successMessage", "User has been edited successfully");
                modelAndView.addObject("buttonType", "Edit");
            } else  {
                userDao.add(user);
                modelAndView.addObject("successMessage", "User has been added successfully");
                modelAndView.addObject("buttonType", "Add");
            }

            modelAndView.addObject("user", new User());
            modelAndView.setViewName("admin/user");
        }

        modelAndView.addObject("admin", authenticationService.userIsAdmin());
        return modelAndView;
    }
}

