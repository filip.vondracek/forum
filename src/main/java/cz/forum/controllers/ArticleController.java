package cz.forum.controllers;

import javax.validation.Valid;

import cz.forum.dao.ArticleDao;
import cz.forum.dao.CategoryDao;
import cz.forum.dao.CommentDao;
import cz.forum.dao.EvaluationDao;
import cz.forum.model.Article;
import cz.forum.model.Comment;
import cz.forum.model.Evaluation;
import cz.forum.services.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

@Controller
public class ArticleController {

    @Autowired
    private ArticleDao articleDao;

    @Autowired
    private CategoryDao categoryDao;

    @Autowired
    private CommentDao commentDao;

    @Autowired
    private EvaluationDao evaluationDao;

    @Autowired
    private AuthenticationService authenticationService;

    @RequestMapping(value = {"/", "/articles"}, method = RequestMethod.GET)
    public ModelAndView articleList(@RequestParam(value = "articleId", required = false) Long id) {
        ModelAndView modelAndView = new ModelAndView();

        if (id != null && authenticationService.isUserAuthenticated()) {
            if (authenticationService.userHasPersmissionsOnArticle(id)) {
                articleDao.remove(id);
                modelAndView.addObject("successMessage", "Article has been deleted successfully");
            } else {
                modelAndView.addObject("successMessage", "You do not have the permission");
            }
        }
        modelAndView.addObject("evaluation", new Evaluation());
        modelAndView.addObject("articles", articleDao.findAll());
        modelAndView.addObject("admin", authenticationService.userIsAdmin());
        modelAndView.addObject("auth", authenticationService.isUserAuthenticated());

        modelAndView.setViewName("articles");
        return modelAndView;
    }

    @PostMapping(value = "/articles")
    public ModelAndView hodnoceni(Evaluation evaluation,
                                  BindingResult bindingResult,
                                  @RequestParam("articleId") long id
    ) {
        ModelAndView modelAndView = new ModelAndView();

        evaluation.setUser(authenticationService.getAuthUser());
        evaluation.setArticle(articleDao.findById(id));
        evaluationDao.add(evaluation);

        modelAndView.addObject("articles", articleDao.findAll());
        modelAndView.addObject("admin", authenticationService.userIsAdmin());
        modelAndView.setViewName("articles");
        modelAndView.addObject("evaluation", new Evaluation());
        modelAndView.addObject("auth", authenticationService.isUserAuthenticated());

        return modelAndView;
    }

    @RequestMapping(value = "/article", method = RequestMethod.GET)
    public ModelAndView showArticle(@RequestParam(value = "articleId", required = false) Long id,
                                    @RequestParam(value = "commentDelId", required = false) Long commentDelId,
                                    @RequestParam(value = "commentEditId", required = false) Long commentEditId) {

        ModelAndView modelAndView = new ModelAndView();

        if (commentDelId != null) {
            if (authenticationService.userHasPersmissionsOnComment(commentDelId)) {
                commentDao.remove(commentDelId);
                modelAndView.addObject("successMessage", "Comment has been deleted successfully");
            } else {
                modelAndView.addObject("successMessage", "You do not have the permission");
            }
        }

        Comment comment = null;
        if (commentEditId != null) {
            if (authenticationService.userHasPersmissionsOnComment(commentEditId)) {
                comment = commentDao.findById(commentEditId);
                modelAndView.addObject("buttonType", "Edit");
            } else {
                modelAndView.addObject("successMessage", "You do not have the permission");
                comment = new Comment();
                modelAndView.addObject("buttonType", "Add");
            }
        } else {
            comment = new Comment();
            modelAndView.addObject("buttonType", "Add");
        }

        modelAndView.addObject("comment", comment);


        modelAndView.addObject("comments", commentDao.findByArticleId(id));
        if (authenticationService.isUserAuthenticated()) {
            modelAndView.addObject("authUserId", authenticationService.getAuthUser().getId());
        }
        modelAndView.addObject("prihlasenyUzivatel", authenticationService.isUserAuthenticated() ? true : false);
        modelAndView.addObject("article", findArticle(id, modelAndView));
        modelAndView.addObject("evaluation", evaluationDao.avgEvaluation(id));

        modelAndView.setViewName("article");
        modelAndView.addObject("admin", authenticationService.userIsAdmin());
        modelAndView.addObject("evaluationByUser", evaluationByUserId());
        modelAndView.addObject("auth", authenticationService.isUserAuthenticated());

        return modelAndView;
    }


    @RequestMapping(value = "/article", method = RequestMethod.POST)
    public ModelAndView commentAction(@Valid Comment comment, BindingResult bindingResult,
                                      @RequestParam(value = "articleId", required = false) Long id) {
        ModelAndView modelAndView = new ModelAndView();

        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("/article");
        } else {
            if (authenticationService.isUserAuthenticated()) {
                modelAndView.addObject("authUserId", authenticationService.getAuthUser().getId());
            }

            if (comment.getCommentId() != 0) {
                comment.setEdited(new Date());
                comment.setArticle(articleDao.findById(id));
                comment.setUser(authenticationService.getAuthUser());
                comment.setCreated(commentDao.findById(comment.getCommentId()).getCreated());
                commentDao.update(comment);
                modelAndView.addObject("successMessage", "Comment has been edited successfully");
                modelAndView.addObject("buttonType", "Edit");
            } else {

                comment.setUser(authenticationService.getAuthUser());
                comment.setCreated(new Date());
                comment.setArticle(articleDao.findById(id));
                commentDao.add(comment);
                modelAndView.addObject("successMessage", "Comment has been added successfully");

                modelAndView.addObject("buttonType", "Add");
            }
            modelAndView.addObject("prihlasenyUzivatel", authenticationService.isUserAuthenticated() ? true : false);

            modelAndView.setViewName("/article");
        }

        modelAndView.addObject("comments", commentDao.findByArticleId(id));
        modelAndView.addObject("article", findArticle(id, modelAndView));
        modelAndView.addObject("evaluationByUser", evaluationByUserId());

        modelAndView.addObject("comment", new Comment());
        modelAndView.addObject("evaluation", evaluationDao.avgEvaluation(id));
        modelAndView.addObject("admin", authenticationService.userIsAdmin());
        modelAndView.addObject("auth", authenticationService.isUserAuthenticated());

        return modelAndView;
    }

    // TODO Zabránit změně id člnáku v url, to stejny u uživatele atd
    @RequestMapping(value = "/user/article", method = RequestMethod.GET)
    public ModelAndView articleForm(@RequestParam(value = "articleId", required = false) Long id) {
        ModelAndView modelAndView = new ModelAndView();
        if (id != null) {
            if (authenticationService.userHasPersmissionsOnArticle(id)) {
                modelAndView.addObject("article", articleDao.findById(id));
                modelAndView.addObject("buttonType", "Edit");
            } else {
                modelAndView.addObject("successMessage", "You do not have the permission");
                modelAndView.addObject("article", new Article());
                modelAndView.addObject("buttonType", "Add");
            }
        } else {
            modelAndView.addObject("article", new Article());
            modelAndView.addObject("buttonType", "Add");
        }
        modelAndView.addObject("categories", categoryDao.findAll());
        modelAndView.setViewName("user/article");
        modelAndView.addObject("admin", authenticationService.userIsAdmin());
        return modelAndView;
    }

    @RequestMapping(value = "/user/article", method = RequestMethod.POST)
    public ModelAndView articleAction(@Valid Article article, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();

        if (bindingResult.hasErrors()) {
            modelAndView.addObject("buttonType", "Add");
            modelAndView.addObject("categories", categoryDao.findAll());
            modelAndView.setViewName("user/article");
        } else {
            if (article.getArticleId() != 0) {
                article.setUser(authenticationService.getAuthUser());
                article.setEdited(new Date());
                article.setCreated(articleDao.findById(article.getArticleId()).getCreated());
                articleDao.update(article);
                modelAndView.addObject("successMessage", "Article has been edited successfully");
                modelAndView.addObject("buttonType", "Edit");
            } else {

                article.setUser(authenticationService.getAuthUser());
                article.setCreated(new Date());
                article.setEdited(new Date());
                articleDao.add(article);
                modelAndView.addObject("successMessage", "Article has been added successfully");

                modelAndView.addObject("buttonType", "Add");
            }

            modelAndView.addObject("article", new Article());
            modelAndView.addObject("categories", categoryDao.findAll());
            modelAndView.setViewName("user/article");

        }
        modelAndView.addObject("admin", authenticationService.userIsAdmin());
        return modelAndView;
    }

    private Article findArticle(Long articleId, ModelAndView modelAndView) {

        Article article = null;
        if (articleId != null) {
            article = articleDao.findById(articleId);
            if (authenticationService.isUserAuthenticated()) {
                if (article.getUser().getId() == authenticationService.getAuthUser().getId()) {
                    modelAndView.addObject("editDelete", true);
                }
            }
        }

        if (article == null) {
            article = new Article();
            article.setArticleId(0);
            article.setTitle("Článek nenalezen");
            article.setText("Je nám líto, ale článke nebyl nalezen");
            article.setCreated(new Date());
            modelAndView.addObject("article", article);

        }
        return article;
    }

    private String evaluationByUserId(){
        if(authenticationService.isUserAuthenticated()){
            if(evaluationDao.evaluationByUser(authenticationService.getAuthUser().getId()) != null){
                return String.valueOf(evaluationDao.evaluationByUser(authenticationService.getAuthUser().getId()).getEvaluationNumber());

            }else{
                return "Nehodnoceno";
            }
        }
        return "Nejste přihlášeni";
    }

}

