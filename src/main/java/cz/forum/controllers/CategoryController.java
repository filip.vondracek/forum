package cz.forum.controllers;

import cz.forum.dao.CategoryDao;
import cz.forum.model.Category;
import cz.forum.services.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class CategoryController {

    @Autowired
    private CategoryDao categoryDao;

    @Autowired
    private AuthenticationService authenticationService;

    @RequestMapping(value="/admin/categories", method = RequestMethod.GET)
    public ModelAndView categoriesList(@RequestParam(value="categoryId",required=false) Long id){
        ModelAndView modelAndView = new ModelAndView();

        if (id!=null) {
            categoryDao.remove(id);
            modelAndView.addObject("successMessage", "Category has been deleted successfully");
        }
        modelAndView.addObject("categories", categoryDao.findAll());
        modelAndView.setViewName("admin/categories");
        modelAndView.addObject("admin", authenticationService.userIsAdmin());
        return modelAndView;
    }


    @RequestMapping(value="/admin/category", method = RequestMethod.GET)
    public ModelAndView categoryForm(@RequestParam(value="categoryId",required=false) Long id){
        ModelAndView modelAndView = new ModelAndView();
        if (id!=null) {
            modelAndView.addObject("category", categoryDao.findById(id));
            modelAndView.addObject("buttonType", "Edit");
        } else {
            modelAndView.addObject("category", new Category());
            modelAndView.addObject("buttonType", "Add");
        }
        modelAndView.setViewName("admin/category");
        modelAndView.addObject("admin", authenticationService.userIsAdmin());
        return modelAndView;
    }

    @RequestMapping(value = "/admin/category", method = RequestMethod.POST)
    public ModelAndView categoryAction(@Valid Category category, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();

        if (bindingResult.hasErrors()) {
            modelAndView.addObject("buttonType", "Add");
            modelAndView.setViewName("admin/category");
        } else {
            if (category.getCategoryId() != 0) {
                categoryDao.update(category);
                modelAndView.addObject("successMessage", "Category has been edited successfully");
                modelAndView.addObject("buttonType", "Edit");
            } else {

                categoryDao.add(category);
                modelAndView.addObject("successMessage", "Category has been added successfully");

                modelAndView.addObject("buttonType", "Add");
            }

            modelAndView.addObject("category", new Category());
            modelAndView.setViewName("admin/category");

        }
        modelAndView.addObject("admin", authenticationService.userIsAdmin());
        return modelAndView;
    }
}

