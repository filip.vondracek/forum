package cz.forum.dao;

import cz.forum.model.Article;
import cz.forum.model.User;

import java.util.List;

public interface ArticleDao {

    void add(Article article);

    List<Article> findAll();

    Article findById(long id);

    void remove(long articleId);

    Article update (Article transientArticle);
}
