package cz.forum.dao;

import cz.forum.model.Evaluation;

public interface EvaluationDao {

    void add(Evaluation evaluation);

    double avgEvaluation(long articleId);

    Evaluation evaluationByUser(int userId);

    void remove(long evaluationId);

    Evaluation findById(long id);


}
