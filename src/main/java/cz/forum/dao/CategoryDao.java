package cz.forum.dao;

import cz.forum.model.Category;

import java.util.List;

public interface CategoryDao {

    void add(Category category);

    List<Category> findAll();

    Category findById(long id);

    void remove(long categoryId);

    Category update(Category transientCategory);
}
