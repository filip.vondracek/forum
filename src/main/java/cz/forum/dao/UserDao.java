package cz.forum.dao;

import cz.forum.model.User;

import java.util.List;

public interface UserDao {

    void add(User user);

    boolean accountExist(User user);

    User findById(int id);

    User findByEmail(String email);

    List<User> findAll();

    void remove(int userId);

    User update (User updatedUser);
}

