package cz.forum.dao;

import cz.forum.model.Role;

public interface RoleDao {

    Role findByRole(String role);

}
