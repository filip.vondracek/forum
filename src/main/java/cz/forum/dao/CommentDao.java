package cz.forum.dao;

import cz.forum.model.Article;
import cz.forum.model.Comment;

import java.util.List;

public interface CommentDao {

    void add(Comment comment);

    Comment findById(long id);

    List<Comment> findByUserId(long userId);

    List<Comment> findByArticleId(long articleId);

    void remove(long commentId);

    Comment update(Comment transientComment);
}
