package cz.forum.impl;

import cz.forum.dao.CommentDao;
import cz.forum.model.Comment;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class CommentDaoImpl implements CommentDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void add(Comment comment) {
        em.persist(comment);
    }

    @Override
    public Comment findById(long id) {
        return em.find(Comment.class, id);
    }

    @Override
    public List<Comment> findByUserId(long userId) {
        try {
            TypedQuery<Comment> query = em.createQuery("SELECT c FROM Comment c " +
                    "join User u on u.id = c.user WHERE u.id = :userId", Comment.class);
            return query.setParameter("userId", userId).getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public List<Comment> findByArticleId(long articleId) {
        try {
            TypedQuery<Comment> query = em.createQuery("SELECT c FROM Comment c " +
                    "join Article a on a.articleId = c.article WHERE a.articleId = :articleId", Comment.class);
            return query.setParameter("articleId", articleId).getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public void remove(long commentId) {
        em.remove(findById(commentId));
    }

    @Override
    public Comment update(Comment transientComment) {
        return em.merge(transientComment);
    }

}

