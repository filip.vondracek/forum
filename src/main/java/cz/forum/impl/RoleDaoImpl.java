package cz.forum.impl;

import cz.forum.dao.RoleDao;
import cz.forum.model.Role;
import cz.forum.model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

@Repository
@Transactional
public class RoleDaoImpl implements RoleDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Role findByRole(String role) {
        try {
            TypedQuery<Role> query = em.createQuery("SELECT r FROM Role r WHERE r.role = :role", Role.class);
            return query.setParameter("role", role).getSingleResult();
        }catch (NoResultException e){
            return null;
        }
    }
}
