package cz.forum.impl;

import cz.forum.dao.EvaluationDao;
import cz.forum.model.Evaluation;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

@Repository
@Transactional
public class EvaluationDaoImpl implements EvaluationDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void add(Evaluation evaluation) {

        if (this.evaluationByUser(evaluation.getUser().getId()) != null) {

            if (evaluation.getEvaluationNumber() == 0) {
                remove(evaluationByUser(evaluation.getUser().getId()).getEvaluationId());
            } else {
                Evaluation transientEvaluation = evaluationByUser(evaluation.getUser().getId());
                transientEvaluation.setEvaluationNumber(evaluation.getEvaluationNumber());
                em.merge(transientEvaluation);
            }
        } else {
            em.persist(evaluation);
        }

    }

    @Override
    public double avgEvaluation(long articleId) {
        try {
            TypedQuery<Double> query = em.createQuery("select avg(e.evaluationNumber) from Evaluation e " +
                    "join Article a on a.articleId = e.article " +
                    "where a.articleId = :articleId", Double.class)
                    .setParameter("articleId", articleId);
            if (query != null && query.getSingleResult() != null) {
                return query.getSingleResult();
            } else {
                return 0;
            }
        } catch (NoResultException e) {
            return 0;
        }
    }

    @Override
    public Evaluation evaluationByUser(int userId) {
        try {
            TypedQuery<Evaluation> query = em.createQuery("select e from Evaluation e " +
                    "join User u on u.id = e.user " +
                    "where u.id = :userId", Evaluation.class)
                    .setParameter("userId", userId);
            if (query != null) {
                return query.getSingleResult();
            } else {
                return null;
            }
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public void remove(long evaluationId) {
        em.remove(findById(evaluationId));
    }

    @Override
    public Evaluation findById(long id) {
        return em.find(Evaluation.class, id);
    }

}

