package cz.forum.impl;

import cz.forum.dao.CategoryDao;
import cz.forum.model.Category;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class CategoryDaoImpl implements CategoryDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void add(Category category) {
        em.persist(category);
    }

    @Override
    public List<Category> findAll() {
        return em.createQuery("SELECT c FROM Category c", Category.class).getResultList();
    }

    @Override
    public Category findById(long categoryId) {
        return em.find(Category.class, categoryId);
    }

    @Override
    public void remove(long categoryId) {
        em.remove(findById(categoryId));
    }

    @Override
    public Category update (Category transientCategory) {
        return em.merge(transientCategory);
    }

}

