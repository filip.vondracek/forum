package cz.forum.impl;

import cz.forum.dao.ArticleDao;
import cz.forum.model.Article;
import cz.forum.model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class ArticleDaoImpl implements ArticleDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void add(Article article) {
        em.persist(article);
    }

    @Override
    public List<Article> findAll() {
        return em.createQuery("SELECT a FROM Article a", Article.class).getResultList();
    }

    @Override
    public Article findById(long articleId) {
        return em.find(Article.class, articleId);
    }

    @Override
    public void remove(long articleId) {
        em.remove(findById(articleId));
    }

    @Override
    public Article update (Article transientArticle) {
        return em.merge(transientArticle);
    }

}

