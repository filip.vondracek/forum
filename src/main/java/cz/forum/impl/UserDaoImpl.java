package cz.forum.impl;

import cz.forum.dao.RoleDao;
import cz.forum.dao.UserDao;
import cz.forum.model.Role;
import cz.forum.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@Repository
@Transactional
public class UserDaoImpl implements UserDao {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private RoleDao roleDao;

    @Override
    public void add(User user) {

        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setActive(1);
        Role userRole = roleDao.findByRole("USER");
        user.setRoles(new HashSet<>(Arrays.asList(userRole)));

        em.persist(user);
    }

    @Override
    public boolean accountExist(User user) {
        User account = findByEmail(user.getEmail());
        if(account == null){
            return false;
        }
        return true;
    }

    @Override
    public User findById(int userId) {
        return em.find(User.class, userId);
    }

    @Override
    public User findByEmail(String email) {
try {
    TypedQuery<User> query = em.createQuery("SELECT u FROM User u WHERE u.email = :email", User.class);
    return query.setParameter("email", email).getSingleResult();
}catch (NoResultException e){
    return null;
}
    }

    @Override
    public List<User> findAll() {

        return em.createQuery("SELECT u FROM User u", User.class).getResultList();
    }

    @Override
    public void remove(int userId) {
        em.remove(findById(userId));
    }

    public User update (User updatedUser) {
        return em.merge(updatedUser);
    }
}

