package cz.forum.log;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Calendar;

@Component
@Aspect
public class LogAspect {

    @Autowired
    FileLogProvider fileLogProvider;

    @Around("execution(* cz.forum.impl.UserDaoImpl.* (..))")
    public Object userLog(ProceedingJoinPoint jp) throws Throwable {
        System.out.println("Uživatel ovlivněn: " + jp.getSignature());
        fileLogProvider.logToFile("Uživatel ovlivněn: " + jp.getSignature() +
                " Time: " + new SimpleDateFormat("yyyy.MM.dd_HH:mm:ss").format(Calendar.getInstance().getTime()));
        Object result = jp.proceed();
        return result;
    }
}
