package cz.forum.log;

import org.springframework.stereotype.Component;

import java.io.*;
import java.util.ArrayList;

@Component
public class FileLogProvider {

    private String logFileName = "userLog.txt";

    public void logToFile(String message) {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(logFileName, true));
            writer.append(message + "\n");

            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<String> getLogFile() {

        ArrayList<String> log = new ArrayList<>();
        String line;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(logFileName));
            while ((line = reader.readLine()) != null) {
                log.add(line);
            }
            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return log;
    }

    public String getLogFileName() {
        return logFileName;
    }

    public void setLogFileName(String logFileName) {
        this.logFileName = logFileName;
    }
}
