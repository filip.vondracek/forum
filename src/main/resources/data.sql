REPLACE INTO `role` VALUES (1,'ADMIN');
REPLACE INTO `role` VALUES (2,'USER');

REPLACE INTO `user` VALUES (100, 1, 'Admin@forum.cz','Admin', 'Admin', '$2a$10$xjH5C5tfyNaGmpZjTJqBMuo3Vgb8c8xcjWZ8VyY6QZs4rWEZxf5li');
INSERT INTO `user`  VALUES (200, 1, 'jana.novakova@email.cz', 'Nováková', 'Jana', '$2a$10$xjH5C5tfyNaGmpZjTJqBMuo3Vgb8c8xcjWZ8VyY6QZs4rWEZxf5li');
REPLACE INTO `user_role` VALUES (100, 1), (200, 2);

INSERT INTO `categories` (`category_id`, `description`, `name`) VALUES
(100, 'Kategorie sloužící k obecné diskuzi.', 'Obecná diskuze'),
(200, 'Kategorie sloužící k diskuzi ohledně novinek.', 'Novinky'),
(300, 'Kategorie sloužicí k řdšení problémů.', 'Problémy'),
(400, 'Kategorie sloužící k diskuzi ohledně možnosti zlešení.', 'Návrhy na zlepšení');

INSERT INTO `articles` (`article_id`, `created`, `edited`, `text`, `title`, `user_id`, `category_id`) VALUES
(100, '2018-12-07 17:38:49', NULL, 'Fusce suscipit libero eget elit. Etiam neque. Nullam at arcu a est sollicitudin euismod. Praesent in mauris eu tortor porttitor accumsan. Donec vitae arcu. Nunc auctor. Praesent dapibus. Cum sociis natoque penatibus et magnis dis parturient montes, nascet', 'První příspěvek', 100, 100),
(200, '2018-12-29 14:20:01', '2019-01-21 08:00:01', 'Fusce suscipit libero eget elit. Etiam neque. Nullam at arcu a est sollicitudin euismod. Praesent in mauris eu tortor porttitor accumsan. Donec vitae arcu. Nunc auctor. Praesent dapibus. Cum sociis natoque penatibus et magnis dis parturient montes, nascet', 'Lorem Ipsum', 200, 200),
(300, '2018-12-29 14:20:10', NULL, 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.', 'Nový Lorem Ipsum', 200, 300);


INSERT INTO `evaluations` (`evaluation_id`, `evaluation_number`, `article_id`, `user_id`) VALUES
(100, 4, 100, 100);


INSERT INTO `comments` (`comment_id`, `created`, `edited`, `text`, `article_id`, `user_id`) VALUES
(100, '2019-01-02 00:00:00', NULL, 'První komentář k článku.', 100, 100),
(200, '2019-01-02 04:00:00', NULL, 'Další komentář....', 100, 200),
(300, '2019-01-14 13:20:27', NULL, 'Koment 1', 200, 100),
(400, '2019-01-21 10:08:47', NULL, 'Velice zajímavý článek.', 300, 200);

