package cz.forum;

import cz.forum.dao.UserDao;
import cz.forum.log.FileLogProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ForumApplicationTests {

    @Autowired
    FileLogProvider fileLogProvider;

    @Autowired
    UserDao userDao;

    private final String TEST_LOG_FILE_NAME = "testLogFile.txt";

    @Test
    public void fileOperationTest() {
        String expected_value = "test";
        String message = "test";
        fileLogProvider.setLogFileName(TEST_LOG_FILE_NAME);
        fileLogProvider.logToFile(message);
        assertEquals(expected_value, fileLogProvider.getLogFile().get(0));
        this.deleteTestFile();
    }

    @Test
    public void userLogTest() {
        fileLogProvider.setLogFileName(TEST_LOG_FILE_NAME);
        userDao.findAll();
        assertEquals(1, fileLogProvider.getLogFile().size());
        this.deleteTestFile();
    }

    private void deleteTestFile() {
        File file = new File(TEST_LOG_FILE_NAME);
        file.delete();
    }

}
